package dariusz.roterman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.kotcrab.vis.ui.VisUI;

import dariusz.roterman.grid.Grid;

public class GameOverChecker extends Actor {

	private Grid userGrid;
	private Grid opponentGrid;

	public GameOverChecker(Grid userGrid, Grid opponentGrid) {
		super();
		this.userGrid = userGrid;
		this.opponentGrid = opponentGrid;
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		int opponentUnitsAlive = opponentGrid.GetAliveUnitsCount();
		int userUnitsAlive = userGrid.GetAliveUnitsCount();
		if (opponentUnitsAlive == 0) {
			Dialog dialog = new Dialog("Congratz", VisUI.getSkin(), "dialog") {
				public void result(Object obj) {
					Gdx.app.exit();
				}
			};
			dialog.text("player won!");
			dialog.button("OK", true);
			dialog.show(getStage());
		}
		if (userUnitsAlive == 0) {
			Dialog dialog = new Dialog(":(", VisUI.getSkin(), "dialog") {
				public void result(Object obj) {
					Gdx.app.exit();
				}
			};
			dialog.text("opponent won!");
			dialog.button("OK", true);
			dialog.show(getStage());
		}

	}

	public Grid getUserGrid() {
		return userGrid;
	}

	public Grid getOpponentGrid() {
		return opponentGrid;
	}

}
