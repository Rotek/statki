package dariusz.roterman.square;

import java.io.Serializable;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import dariusz.roterman.unit.Unit;

public abstract class Square extends Actor implements Serializable {
	private static final long serialVersionUID = 988839043893989298L;

	protected static final Texture UNIT_TEXTURE = new Texture("grass.png");
	protected static final Texture CROSSHAIR_TEXTURE = new Texture("crosshair.png");

	private final SquareType squareType;
	private Unit unit;
	private boolean destroyed;

	public Square(SquareType squareType, boolean destroyed) {
		this.squareType = squareType;
		this.destroyed = destroyed;
	}

	public abstract void draw(Batch batch, float parentAlpha);

	public SquareType getSquareType() {
		return squareType;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public boolean isDestroyed() {
		return destroyed;
	}

	public void setDestroyed(boolean destroyed) {
		this.destroyed = destroyed;
	}

}
