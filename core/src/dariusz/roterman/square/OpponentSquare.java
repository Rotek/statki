package dariusz.roterman.square;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import dariusz.roterman.GameInfo;
import dariusz.roterman.GameState;

public class OpponentSquare extends Square {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2416425787459883494L;

	public OpponentSquare(SquareType squareType, boolean destroyed) {
		super(squareType, destroyed);
		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (GameInfo.INSTANCE.getGameState() == GameState.USER_TURN && !isDestroyed()) {
					setDestroyed(true);
					GameInfo.INSTANCE.setShots(GameInfo.INSTANCE.getShots() + 1);
					GameInfo.INSTANCE.setGameState(GameState.OPPONENT_TURN);
				}
			}
		});

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (isDestroyed()) {
			if (getUnit() != null) {
				batch.draw(UNIT_TEXTURE, getX(), getY(), getWidth(), getHeight());
			} else {
				batch.draw(getSquareType().getTexture(), getX(), getY(), getWidth(), getHeight());
			}
			batch.draw(CROSSHAIR_TEXTURE, getX(), getY(), getWidth(), getHeight());
		} else {
			batch.draw(getSquareType().getTexture(), getX(), getY(), getWidth(), getHeight());
		}
	}
}
