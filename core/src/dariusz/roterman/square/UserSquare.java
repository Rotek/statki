package dariusz.roterman.square;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;

public class UserSquare extends Square {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4313715768188318247L;

	public UserSquare(SquareType squareType, boolean destroyed) {
		super(squareType, destroyed);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		Texture texture;
		if (getUnit() != null) {
			texture = UNIT_TEXTURE;
		} else {
			texture = getSquareType().getTexture();
		}
		batch.draw(texture, getX(), getY(), getWidth(), getHeight());
		if (isDestroyed()) {
			batch.draw(CROSSHAIR_TEXTURE, getX(), getY(), getWidth(), getHeight());
		}
	}
}
