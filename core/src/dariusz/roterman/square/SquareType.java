package dariusz.roterman.square;

import com.badlogic.gdx.graphics.Texture;

public enum SquareType {

	SEA(new Texture("sea.png")), GROUND(new Texture("ground.png"));

	private Texture texture;

	private SquareType(Texture texture) {
		this.texture = texture;
	}

	public Texture getTexture() {
		return texture;
	}

}
