package dariusz.roterman;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kotcrab.vis.ui.VisUI;

import dariusz.roterman.screen.UnitAssignmentScreenRenderer;

public class GdxGame extends ApplicationAdapter {

	private Stage stage;

	@Override
	public void create() {
		VisUI.load();

		stage = new Stage();
		Gdx.input.setInputProcessor(stage);

		stage.addActor(new UnitAssignmentScreenRenderer(stage));

	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

	@Override
	public void dispose() {
		VisUI.dispose();
	}
}
