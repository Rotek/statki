package dariusz.roterman.screen;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooser.Mode;
import com.kotcrab.vis.ui.widget.file.FileChooser.SelectionMode;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;

import dariusz.roterman.Configuration;
import dariusz.roterman.GameInfo;
import dariusz.roterman.GameOverChecker;
import dariusz.roterman.GameState;
import dariusz.roterman.grid.AwkwardEnviromentCreationStrategy;
import dariusz.roterman.grid.Grid;
import dariusz.roterman.grid.GridFactory;
import dariusz.roterman.grid.GridRenderer;
import dariusz.roterman.grid.GridToTextSerializer;
import dariusz.roterman.grid.GridType;
import dariusz.roterman.opponent.Opponent;
import dariusz.roterman.opponent.RandomShootStrategy;
import dariusz.roterman.unit.RandomUnitToGridAssignmentStrategy;
import dariusz.roterman.unit.UnableToFitUnitsIntoGridException;
import dariusz.roterman.unit.UnitToGridAssigner;

public class UnitAssignmentScreenRenderer extends Group {
	private Grid userGrid;
	private GridRenderer userGridRenderer;
	private GridFactory gridFactory;
	private GridToTextSerializer gridToTextSerializer;
	private FileChooser fileChooser;
	private UnitToGridAssigner unitToGridAssigner;
	private Configuration configuration;

	public UnitAssignmentScreenRenderer(final Stage stage) {
		gridFactory = new GridFactory(new AwkwardEnviromentCreationStrategy());
		userGrid = gridFactory.getGrid(22, 14, 50, GridType.USER);
		userGridRenderer = new GridRenderer(userGrid, 17, 1);
		unitToGridAssigner = new UnitToGridAssigner(new RandomUnitToGridAssignmentStrategy());
		configuration = new Configuration();

		Button playButton = new VisTextButton("Play");
		Button saveButton = new VisTextButton("save");
		Button loadButton = new VisTextButton("load");
		Button assignUnitsButton = new VisTextButton("reassign");
		Button creditsButton = new VisTextButton("credits");
		gridToTextSerializer = new GridToTextSerializer();

		FileChooser.setFavoritesPrefsName("dariusz.roterman");
		fileChooser = new FileChooser(Mode.SAVE);
		saveButton.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {

				final String serializedGrid = gridToTextSerializer.serialize(userGrid);

				fileChooser.setMode(Mode.SAVE);
				fileChooser.setSelectionMode(SelectionMode.FILES);
				fileChooser.setListener(new FileChooserAdapter() {
					@Override
					public void selected(FileHandle file) {
						PrintWriter writer = null;
						try {
							writer = new PrintWriter(file.file().getAbsolutePath(), "ISO-8859-1");
						} catch (FileNotFoundException e) {
							Dialog dialog = new Dialog("Warning", VisUI.getSkin(), "dialog") {
								public void result(Object obj) {
								}
							};
							dialog.text("file cannot be found");
							dialog.button("OK", true);
							dialog.show(stage);
						} catch (UnsupportedEncodingException e) {
							Dialog dialog = new Dialog("Warning", VisUI.getSkin(), "dialog") {
								public void result(Object obj) {
								}
							};
							dialog.text("encoding is unsupported");
							dialog.button("OK", true);
							dialog.show(stage);
						}
						writer.print(serializedGrid);
						writer.close();
					}
				});
				fileChooser.setSize(480, 740);
				stage.addActor(fileChooser.fadeIn());
			}
		});
		loadButton.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {

				fileChooser.setMode(Mode.OPEN);
				fileChooser.setSelectionMode(SelectionMode.FILES);
				fileChooser.setListener(new FileChooserAdapter() {
					@Override
					public void selected(FileHandle file) {
						Path path = Paths.get(file.file().getAbsolutePath());
						byte[] bytes = null;
						try {
							bytes = Files.readAllBytes(path);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						String serializedGrid = new String(bytes, StandardCharsets.ISO_8859_1);
						Grid grid = gridToTextSerializer.deserialize(serializedGrid);
						userGrid = grid;
						userGridRenderer.changeGrid(userGrid);
					}
				});
				fileChooser.setSize(480, 740);
				stage.addActor(fileChooser.fadeIn());
			}
		});

		assignUnitsButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				try {
					userGrid.unassignUnits();
					unitToGridAssigner.Assign(userGrid, configuration.getUnitsSetUp());
				} catch (UnableToFitUnitsIntoGridException e) {
					Dialog dialog = new Dialog("Warning", VisUI.getSkin(), "dialog") {
						public void result(Object obj) {
						}
					};
					dialog.text("Units cannot be assigned to current map");
					dialog.button("OK", true);
					dialog.show(stage);
				}
			}
		});
		playButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				try {
					Grid opponentGrid = gridFactory.getGrid(22, 14, 50, GridType.OPPONENT);
					unitToGridAssigner.Assign(opponentGrid, configuration.getUnitsSetUp());
					GridRenderer opponentGridRenderer = new GridRenderer(opponentGrid, 17, 1);
					if (userGrid.getUnits() == null || opponentGrid.getUnits() == null) {
						throw new UnitsNotAssignedException();
					}
					clear();
					Opponent opponent = new Opponent(userGrid, new RandomShootStrategy());
					GameScreenRenderer gameScreenRenderer = new GameScreenRenderer(userGridRenderer,
							opponentGridRenderer, opponent, new GameOverChecker(userGrid, opponentGrid));
					GameInfo.INSTANCE.setGameState(GameState.USER_TURN);
					stage.addActor(gameScreenRenderer);
				} catch (UnableToFitUnitsIntoGridException e) {
					Dialog dialog = new Dialog("Warning", VisUI.getSkin(), "dialog") {
						public void result(Object obj) {
						}
					};
					dialog.text("Units cannot be assigned to current opponent map");
					dialog.button("OK", true);
					dialog.show(stage);
				} catch (UnitsNotAssignedException e) {
					Dialog dialog = new Dialog("Warning", VisUI.getSkin(), "dialog") {
						public void result(Object obj) {
						}
					};
					dialog.text("some map does not contain any units");
					dialog.button("OK", true);
					dialog.show(stage);
				}
			}
		});
		creditsButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Dialog dialog = new Dialog("Credits", VisUI.getSkin(), "dialog") {
					public void result(Object obj) {
					}
				};
				dialog.text(
						"Credits for fluxord (opengameart.org) for crosshair textures\nand Kenney (opengameart.org) for Enviroment textures\n"
								+ "Coded by Dariusz Roterman IO2");
				dialog.button("OK I appreciate", true);
				dialog.show(stage);
			}
		});

		userGridRenderer.setPosition(0, 0);

		playButton.setPosition(400, 0);
		playButton.setSize(80, 50);
		saveButton.setPosition(400, 65);
		saveButton.setSize(80, 50);
		loadButton.setPosition(400, 130);
		loadButton.setSize(80, 50);
		assignUnitsButton.setPosition(400, 195);
		assignUnitsButton.setSize(80, 50);
		creditsButton.setPosition(400, 260);
		creditsButton.setSize(80, 50);

		addActor(userGridRenderer);
		addActor(assignUnitsButton);
		addActor(saveButton);
		addActor(loadButton);
		addActor(playButton);
		addActor(creditsButton);
	}

}
