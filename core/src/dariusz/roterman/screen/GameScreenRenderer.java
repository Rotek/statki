package dariusz.roterman.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kotcrab.vis.ui.widget.VisLabel;

import dariusz.roterman.GameInfo;
import dariusz.roterman.GameOverChecker;
import dariusz.roterman.grid.GridRenderer;
import dariusz.roterman.opponent.Opponent;

public class GameScreenRenderer extends Group {
	private final GridRenderer playerGridRenderer;
	private final GridRenderer opponentGridRenderer;
	private Label opponentUnitsCountLabel;
	private VisLabel userUnitsCountLabel;
	private VisLabel userShotsCountLabel;

	public GameScreenRenderer(GridRenderer playerGridRenderer, GridRenderer opponentGridRenderer, Opponent opponent,
			GameOverChecker gameOverChecker) {
		this.playerGridRenderer = playerGridRenderer;
		this.opponentGridRenderer = opponentGridRenderer;

		Label opponentUnitsCountTitleLabel = new VisLabel("Units", new Color(0, 0, 0, 1));
		opponentUnitsCountTitleLabel.setPosition(400, 30);
		opponentUnitsCountLabel = new VisLabel("", new Color(0, 0, 0, 1));
		opponentUnitsCountLabel.setPosition(400, 10);

		Label userUnitsCountTitleLabel = new VisLabel("Units", new Color(0, 0, 0, 1));
		userUnitsCountTitleLabel.setPosition(400, 330);
		userUnitsCountLabel = new VisLabel("", new Color(0, 0, 0, 1));
		userUnitsCountLabel.setPosition(400, 310);

		Label userShotsTitleLabel = new VisLabel("Shots", new Color(0, 0, 0, 1));
		userShotsTitleLabel.setPosition(400, 370);
		userShotsCountLabel = new VisLabel("0", new Color(0, 0, 0, 1));
		userShotsCountLabel.setPosition(400, 350);

		Label userGridLabel = new VisLabel("User",new Color(0, 0, 0, 1));
		userGridLabel.setPosition(0, 550);
		Label opponentGridLabel = new VisLabel("Opponent",new Color(0, 0, 0, 1));
		opponentGridLabel.setPosition(0, 250);

		opponentGridRenderer.setPosition(0, 0);
		playerGridRenderer.setPosition(0, 300);
		addActor(playerGridRenderer);
		addActor(opponentGridRenderer);
		addActor(opponent);
		addActor(opponentUnitsCountLabel);
		addActor(opponentUnitsCountTitleLabel);
		addActor(userUnitsCountTitleLabel);
		addActor(userUnitsCountLabel);
		addActor(userShotsTitleLabel);
		addActor(userShotsCountLabel);
		addActor(gameOverChecker);
		addActor(userGridLabel);
		addActor(opponentGridLabel);
	}

	@Override
	public void act(float delta) {
		int opponentUnitsAlive = opponentGridRenderer.getGrid().GetAliveUnitsCount();
		opponentUnitsCountLabel.setText("" + opponentUnitsAlive);
		int userUnitsAlive = playerGridRenderer.getGrid().GetAliveUnitsCount();
		userUnitsCountLabel.setText("" + userUnitsAlive);
		userShotsCountLabel.setText(GameInfo.INSTANCE.getShots() + "");
		super.act(delta);
	}

}
