package dariusz.roterman.opponent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dariusz.roterman.grid.Grid;
import dariusz.roterman.square.Square;

public class RandomShootStrategy implements OpponentshootStrategy {

	@Override
	public Square getSquareToShoot(Grid grid) {
		Square[][] squares = grid.getSquares();
		List<Integer> columns = new ArrayList<Integer>();
		for (int column = 0; column < squares.length; column++) {
			columns.add(column);
		}
		List<Integer> rows = new ArrayList<Integer>();
		for (int row = 0; row < squares[0].length; row++) {
			rows.add(row);
		}
		Collections.shuffle(columns);
		Collections.shuffle(rows);
		for (Integer column : columns) {
			for (Integer row : rows) {
				Square square = squares[column][row];
				if (!square.isDestroyed()) {
					return square;
				}
			}
		}
		return null;
	}

}
