package dariusz.roterman.opponent;

import dariusz.roterman.grid.Grid;
import dariusz.roterman.square.Square;

public interface OpponentshootStrategy {

	public Square getSquareToShoot(Grid grid);
}
