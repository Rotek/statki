package dariusz.roterman.opponent;

import com.badlogic.gdx.scenes.scene2d.Actor;

import dariusz.roterman.GameInfo;
import dariusz.roterman.GameState;
import dariusz.roterman.grid.Grid;

public class Opponent extends Actor {
	Grid playerGrid;
	OpponentshootStrategy opponentshootStrategy;

	public Opponent(Grid playerGrid, OpponentshootStrategy opponentshootStrategy) {
		super();
		this.playerGrid = playerGrid;
		this.opponentshootStrategy = opponentshootStrategy;
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		if (GameInfo.INSTANCE.getGameState() == GameState.OPPONENT_TURN) {
			opponentshootStrategy.getSquareToShoot(playerGrid).setDestroyed(true);
			GameInfo.INSTANCE.setGameState(GameState.USER_TURN);
		}
	}
}
