package dariusz.roterman.grid;

import com.badlogic.gdx.scenes.scene2d.Group;

import dariusz.roterman.square.Square;

public class GridRenderer extends Group {

	private Grid grid;
	private int squareSize;
	private int gapSize;

	public GridRenderer(Grid grid, int squareSize, int gapSize) {
		this.grid = grid;
		this.squareSize = squareSize;
		this.gapSize = gapSize;

		for (int column = 0; column < grid.getColumnsCount(); column++) {
			for (int row = 0; row < grid.getRowsCount(); row++) {
				Square square = grid.getSquares()[column][row];
				int squareX = column * (squareSize + gapSize);
				int squareY = row * (squareSize + gapSize);
				square.setPosition(squareX, squareY);
				square.setSize(squareSize, squareSize);
				addActor(square);
			}
		}
	}
public void changeGrid(Grid grid) {
	this.grid = grid;
	clearChildren();
	
	for (int column = 0; column < grid.getColumnsCount(); column++) {
		for (int row = 0; row < grid.getRowsCount(); row++) {
			Square square = grid.getSquares()[column][row];
			int squareX = column * (squareSize + gapSize);
			int squareY = row * (squareSize + gapSize);
			square.setPosition(squareX, squareY);
			square.setSize(squareSize, squareSize);
			addActor(square);
		}
	}
}
	public int getSquareSize() {
		return squareSize;
	}

	public void setSquareSize(int squareSize) {
		this.squareSize = squareSize;
	}

	public int getGapSize() {
		return gapSize;
	}

	public void setGapSize(int gapSize) {
		this.gapSize = gapSize;
	}
	public Grid getGrid() {
		return grid;
	}
	public void setGrid(Grid grid) {
		this.grid = grid;
	}

}
