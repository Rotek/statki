package dariusz.roterman.grid;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;

public class GridToTextSerializer {
	public String serialize(Grid grid) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(grid);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("grid cannot be serialized WTF");
		}
		return Base64.getEncoder().encodeToString(baos.toByteArray());
	}

	public Grid deserialize(String string) {
		byte[] bytes = Base64.getDecoder().decode(string);
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Object object = null;
		try {
			ObjectInputStream ois = new ObjectInputStream(bais);
			object = ois.readObject();
		} catch (Exception e) {
			System.err.println("grid cannot be deserialized WTF");
		}
		return (Grid) object;
	}
}
