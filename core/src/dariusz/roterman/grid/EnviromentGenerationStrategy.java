package dariusz.roterman.grid;

import dariusz.roterman.square.SquareType;

public interface EnviromentGenerationStrategy {

	/**
	 * @param collumns
	 *            number of columns
	 * @param rows
	 *            number of rows
	 * @param percentOfSea
	 *            percent of sea [0 to 100]
	 * @return Enviroment as a Table of SquareType
	 */
	public SquareType[][] generateEnviroment(int collumns, int rows, int percentOfSea);

}
