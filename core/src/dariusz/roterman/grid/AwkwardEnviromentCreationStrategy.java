package dariusz.roterman.grid;

import dariusz.roterman.square.SquareType;

public class AwkwardEnviromentCreationStrategy implements EnviromentGenerationStrategy {

	@Override
	public SquareType[][] generateEnviroment(int columns, int rows, int percentOfSea) {
		SquareType[][] squareTypes = initializeTable(columns, rows, SquareType.GROUND);

		int seaSquaresCount = (columns * rows * percentOfSea) / 100;
		outer: for (int i = 0; i < columns; i++) {
			for (int j = 0; j <= i && j < rows; j++) {
				squareTypes[i][j] = SquareType.SEA;
				seaSquaresCount--;
				if (seaSquaresCount <= 0) {
					break outer;
				}
			}
		}
		return squareTypes;
	}

	private SquareType[][] initializeTable(int columns, int rows, SquareType value) {
		SquareType[][] squareTypes = new SquareType[columns][rows];
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				squareTypes[i][j] = value;
			}
		}
		return squareTypes;
	}

}
