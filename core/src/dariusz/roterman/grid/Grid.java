package dariusz.roterman.grid;

import java.io.Serializable;
import java.util.List;

import dariusz.roterman.square.Square;
import dariusz.roterman.unit.Unit;

public class Grid implements Serializable {

	private static final long serialVersionUID = -2668573890762777354L;

	private final Square[][] squares;
	private final int columnsCount;
	private final int rowsCount;
	private List<Unit> units;

	public Grid(Square[][] squares, int columnsCount, int rowsCount) {
		this.squares = squares;
		this.columnsCount = columnsCount;
		this.rowsCount = rowsCount;
	}

	public Square[][] getSquares() {
		return squares;
	}

	public int getColumnsCount() {
		return columnsCount;
	}

	public int getRowsCount() {
		return rowsCount;
	}

	public List<Unit> getUnits() {
		return units;
	}

	public void setUnits(List<Unit> units) {
		this.units = units;
	}

	public void unassignUnits() {
		units = null;
		for (int column = 0; column < getColumnsCount(); column++) {
			for (int row = 0; row < getRowsCount(); row++) {
				squares[column][row].setUnit(null);
			}
		}
	}

	public int GetAliveUnitsCount() {
		int unitsAlive = 0;
		for (Unit unit : getUnits()) {
			if (!unit.isDestroyed()) {
				unitsAlive++;
			}
		}
		return unitsAlive;
	}
}