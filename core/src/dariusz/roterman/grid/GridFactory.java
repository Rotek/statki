package dariusz.roterman.grid;

import dariusz.roterman.square.OpponentSquare;
import dariusz.roterman.square.Square;
import dariusz.roterman.square.SquareType;
import dariusz.roterman.square.UserSquare;

public class GridFactory {

	private final EnviromentGenerationStrategy enviromentGenerationStrategy;

	public GridFactory(EnviromentGenerationStrategy gridCreatingStrategy) {
		this.enviromentGenerationStrategy = gridCreatingStrategy;
	}

	public Grid getGrid(int columns, int rows, int percentOfSea, GridType gridType) {
		Square[][] squares = new Square[columns][rows];
		SquareType[][] squareTypes = enviromentGenerationStrategy.generateEnviroment(columns, rows, percentOfSea);
		for (int column = 0; column < columns; column++) {
			for (int row = 0; row < rows; row++) {
				Square square = null;
				switch (gridType) {
				case OPPONENT:
					square = new OpponentSquare(squareTypes[column][row], false);
					break;
				case USER:
					square = new UserSquare(squareTypes[column][row], false);
					break;

				default:
					break;
				}
				squares[column][row] = square;
			}
		}
		return new Grid(squares, columns, rows);
	}
}
