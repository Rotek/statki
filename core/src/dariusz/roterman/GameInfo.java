package dariusz.roterman;

public enum GameInfo {
	INSTANCE(GameState.UNIT_ASIGNING, 0);

	private GameState gameState;
	private int shots;

	private GameInfo(GameState gameState, int shots) {
		this.gameState = gameState;
		this.setShots(shots);
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public int getShots() {
		return shots;
	}

	public void setShots(int shots) {
		this.shots = shots;
	}
}
