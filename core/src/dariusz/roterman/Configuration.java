package dariusz.roterman;

import java.util.HashMap;
import java.util.Map;

import dariusz.roterman.unit.UnitType;

public class Configuration {
	private Map<UnitType, Integer> unitsSetUp;

	public Configuration() {
		unitsSetUp = new HashMap<UnitType, Integer>();
		unitsSetUp.put(UnitType.TANK, 2);
		unitsSetUp.put(UnitType.ROCKET_LAUNCHER, 2);
		unitsSetUp.put(UnitType.BATTLESHIP, 2);
		unitsSetUp.put(UnitType.SUBMARINE, 3);
		unitsSetUp.put(UnitType.PATROL_BOAT, 4);
		unitsSetUp.put(UnitType.PLANE, 2);
	}

	public Map<UnitType, Integer> getUnitsSetUp() {
		return unitsSetUp;
	}

}
