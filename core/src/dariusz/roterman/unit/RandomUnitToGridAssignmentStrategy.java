package dariusz.roterman.unit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import dariusz.roterman.grid.Grid;
import dariusz.roterman.square.Square;

public class RandomUnitToGridAssignmentStrategy extends AbstractUnitToGridAssignmentStrategy {

	@Override
	protected Square[][] findProperSquaresForUnit(Grid grid, UnitType unitType)
			throws UnableToFitUnitsIntoGridException {
		Square[][] squares = grid.getSquares();
		List<Integer> columns = new ArrayList<Integer>();
		for (int column = 0; column < squares.length; column++) {
			columns.add(column);
		}
		List<Integer> rows = new ArrayList<Integer>();
		for (int row = 0; row < squares[0].length; row++) {
			rows.add(row);
		}
		List<Integer> rotations = Arrays.asList(new Integer[] { 0, 1, 2, 3 });

		Collections.shuffle(columns);
		Collections.shuffle(rows);
		for (Integer column : columns) {
			for (Integer row : rows) {
				Square square = squares[column][row];
				Collections.shuffle(rotations);
				for (Integer rotation : rotations) {
					unitType.setRotation(rotation);
					if (square.getUnit() == null) {
						int rowsLeft = squares[column].length - row;
						int columnsLeft = squares.length - column;
						if (unitType.getShape().length > columnsLeft) {
							continue;
						}
						if (unitType.getShape()[0].length > rowsLeft) {
							continue;
						}
						int columnEnd = column + unitType.getShape().length;
						int rowEnd = row + unitType.getShape()[0].length;
						if (doesMatch(squares, column, columnEnd, row, rowEnd, unitType)) {
							Square[][] section = createSubMatrix(squares, column, row, columnEnd, rowEnd);
							return section;
						}
					}
				}
			}
		}
		throw new UnableToFitUnitsIntoGridException();
	}

}
