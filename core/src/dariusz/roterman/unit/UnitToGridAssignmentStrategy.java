package dariusz.roterman.unit;

import java.util.Map;

import dariusz.roterman.grid.Grid;

public interface UnitToGridAssignmentStrategy {


	public void AssignUnitsToGrid(Grid grid, Map<UnitType, Integer> remainingUnits) throws UnableToFitUnitsIntoGridException;
}
