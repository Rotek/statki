package dariusz.roterman.unit;

import java.io.Serializable;
import java.util.List;

import dariusz.roterman.square.Square;

public class Unit implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7058165046615102251L;
	private List<Square> squares;
	private final UnitType unitType;

	public Unit(UnitType unitType) {
		this.unitType = unitType;
	}

	public boolean isDestroyed() {
		for (Square square : squares) {
			if (!square.isDestroyed()) {
				return false;
			}
		}
		return true;
	}

	public List<Square> getSquares() {
		return squares;
	}

	public void setSquares(List<Square> squares) {
		this.squares = squares;
	}

	public UnitType getUnitType() {
		return unitType;
	}

}
