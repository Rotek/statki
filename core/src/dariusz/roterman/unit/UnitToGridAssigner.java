package dariusz.roterman.unit;

import java.util.Map;

import dariusz.roterman.grid.Grid;

public class UnitToGridAssigner {

	private UnitToGridAssignmentStrategy assignmentStrategy;

	public UnitToGridAssigner(UnitToGridAssignmentStrategy assignmentStrategy) {
		this.assignmentStrategy = assignmentStrategy;
	}

	public void Assign(Grid grid, Map<UnitType, Integer> remainingUnits) throws UnableToFitUnitsIntoGridException {
		assignmentStrategy.AssignUnitsToGrid(grid, remainingUnits);
	}
}
