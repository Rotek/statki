package dariusz.roterman.unit;

import dariusz.roterman.grid.Grid;
import dariusz.roterman.square.Square;

public class NaiveUnitToGridAssignmentStrategy extends AbstractUnitToGridAssignmentStrategy {
	@Override
	protected Square[][] findProperSquaresForUnit(Grid grid, UnitType unitType)
			throws UnableToFitUnitsIntoGridException {
		Square[][] squares = grid.getSquares();
		outer: for (int column = 0; column < squares.length; column++) {
			for (int row = 0; row < squares[column].length; row++) {
				Square square = squares[column][row];
				if (square.getUnit() == null) {
					int rowsLeft = squares[column].length - row;
					int columnsLeft = squares.length - column;
					if (unitType.getShape().length > columnsLeft) {
						break outer;
					}
					if (unitType.getShape()[0].length > rowsLeft) {
						continue outer;
					}
					int columnEnd = column + unitType.getShape().length;
					int rowEnd = row + unitType.getShape()[0].length;
					if (doesMatch(squares, column, columnEnd, row, rowEnd, unitType)) {
						Square[][] section = createSubMatrix(squares, column, row, columnEnd, rowEnd);
						return section;
					}

				}
			}
		}
		throw new UnableToFitUnitsIntoGridException();
	}
}
