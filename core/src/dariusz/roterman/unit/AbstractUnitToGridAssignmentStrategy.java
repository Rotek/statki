package dariusz.roterman.unit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import dariusz.roterman.grid.Grid;
import dariusz.roterman.square.Square;

public abstract class AbstractUnitToGridAssignmentStrategy implements UnitToGridAssignmentStrategy {

	@Override
	public void AssignUnitsToGrid(Grid grid, Map<UnitType, Integer> remainingUnits) throws  UnableToFitUnitsIntoGridException{

		List<Unit> units = new ArrayList<Unit>(remainingUnits.size());
		for (UnitType unitType : remainingUnits.keySet()) {
			for (int unitCount = 0; unitCount < remainingUnits.get(unitType); unitCount++) {
				Unit unit = new Unit(unitType);
				Square[][] section = findProperSquaresForUnit(grid, unit.getUnitType());

				units.add(unit);
				assign(section, unit);
			}
		}
		grid.setUnits(units);
	}

	protected abstract Square[][] findProperSquaresForUnit(Grid grid, UnitType unitType)
			throws UnableToFitUnitsIntoGridException;

	protected Square[][] createSubMatrix(Square[][] squares, int column, int row, int columnEnd, int rowEnd) {
		Square[][] section = new Square[columnEnd - column][rowEnd - row];
		for (int i = 0; i < section.length; i++) {
			section[i] = Arrays.copyOfRange(squares[column], row, rowEnd);
			column++;
		}
		return section;
	}

	protected boolean doesMatch(Square[][] section, int columnStart, int columnEnd, int rowStart, int rowEnd,
			UnitType unitType) {
		for (int column = columnStart; column < columnEnd; column++) {
			for (int row = rowStart; row < rowEnd; row++) {
				if (unitType.getShape()[column - columnStart][row - rowStart] == true) {
					if (doNearestSquaresHaveUnit(section, column, row)) {
						return false;
					}
					if (section[column][row].getUnit() != null) {
						return false;
					}
					if (!hasProperSquareType(section[column][row], unitType)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private boolean hasProperSquareType(Square square, UnitType unitType) {
		return unitType.getAvaibleSquareTypes().contains(square.getSquareType());
	}

	private boolean doNearestSquaresHaveUnit(Square[][] squares, int squareColumn, int squareRow) {
		for (int column = squareColumn - 1; column <= squareColumn + 1; column++) {
			for (int row = squareRow - 1; row <= squareRow + 1; row++) {
				if (isOutOfBounds(squares, column, row)) {
					continue;
				}
				if (squares[column][row].getUnit() != null) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isOutOfBounds(Object[][] table, int column, int row) {
		if (table.length <= column || column < 0) {
			return true;
		}
		if (table[column].length <= row || row < 0) {
			return true;
		}
		return false;
	}

	private void assign(Square[][] section, Unit unit) {
		UnitType unitType = unit.getUnitType();
		List<Square> squaresForUnit = new ArrayList<Square>();
		for (int column = 0; column < section.length; column++) {
			for (int row = 0; row < section[0].length; row++) {
				if (unitType.getShape()[column][row] == true) {
					squaresForUnit.add(section[column][row]);
					section[column][row].setUnit(unit);
				}
			}
		}
		unit.setSquares(squaresForUnit);
	}

}