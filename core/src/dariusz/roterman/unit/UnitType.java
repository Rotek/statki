package dariusz.roterman.unit;

import java.util.Arrays;
import java.util.List;

import dariusz.roterman.square.SquareType;

public enum UnitType {

	TANK(new boolean[][] { { true, true }, { true, true } }, Arrays.asList(SquareType.GROUND)),

	ROCKET_LAUNCHER(new boolean[][] { { true, true, true }, { true, true, true } }, Arrays.asList(SquareType.GROUND)),

	BATTLESHIP(new boolean[][] { { true }, { true }, { true }, { true } }, Arrays.asList(SquareType.SEA)),

	SUBMARINE(new boolean[][] { { true }, { true }, { true } }, Arrays.asList(SquareType.SEA)),

	PATROL_BOAT(new boolean[][] { { true }, { true } }, Arrays.asList(SquareType.SEA)),

	PLANE(new boolean[][] { { false, false, true }, { true, true, true }, { false, false, true } },
			Arrays.asList(SquareType.values()));

	private final boolean[][] shape;
	private final List<SquareType> avaibleSquareTypes;
	private int rotation = 0;

	private UnitType(boolean[][] squares, List<SquareType> avaibleSquareTypes) {
		this.shape = squares;
		this.avaibleSquareTypes = avaibleSquareTypes;
	}

	public boolean[][] getShape() {
		return RotateMatrix90(shape, rotation);
	}

	public List<SquareType> getAvaibleSquareTypes() {
		return avaibleSquareTypes;
	}

	private boolean[][] RotateMatrix90(boolean[][] matrix, int times) {
		boolean[][] rotated = Arrays.copyOf(matrix, matrix.length);
		for (int i = 0; i < times; i++) {
			rotated = RotateMatrix90(rotated);
		}
		return rotated;
	}

	private boolean[][] RotateMatrix90(boolean[][] matrix) {
		boolean[][] rotated = new boolean[matrix[0].length][matrix.length];

		for (int i = 0; i < matrix[0].length; ++i) {
			for (int j = 0; j < matrix.length; ++j) {
				rotated[i][j] = matrix[matrix.length - j - 1][i];
			}
		}

		return rotated;
	}

	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = rotation;
	}
}
